FROM ubuntu:focal

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
USER gitpod
RUN brew install bastet
RUN sudo apt-get update \
    && sudo apt-get install -y \
    && sudo apt-get install texlive-latex-extra
    